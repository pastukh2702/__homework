/*
  Задание:

  Написать класс SuperDude который как аргумент принимает два параметра:
    - Имя
    - Массив суперспособностей которые являются обьектом.

    Модель суперспособности:
      {
        // Имя способности
        name:'Invisibility',
        // Сообщение которое будет выведено когда способность была вызвана
        spell: function(){ return `${this.name} hide from you`}
      }

    В конструкторе, нужно:
    - сделать так, что бы имя нельзя было перезаписывать и присвоить ему то
      значение которое мы передали как аргумент.

    - перебрать массив способностей и на каждую из них создать метод для этого
      обьекта, используя поле name как название метода, а spell как то,
      что нужно вернуть в console.log при вызове этого метода.
    - все способности должны быть неизменяемые

    - бонус, создать конструктор суперспособностей -> new Spell( name, spellFunc );
*/
let superPowers = [
  { name:'Invisibility', spell: function(){ return `${this.name} hide from you`} },
  { name:'superSpeed', spell: function(){ return `${this.name} running from you`} },
  { name:'superSight', spell: function(){ return `${this.name} see you`} },
  { name:'superFroze', spell: function(){ return `${this.name} will froze you`} },
  { name:'superSkin',  spell: function(){ return `${this.name} skin is unbreakable`} },
];
superPowers.forEach((power)=>{
  Object.defineProperty(power, 'name', {
    writable: false,
    enumerable: false
  });
  Object.defineProperty(power, 'spell', {
    writable: false,
    enumerable: false
  });
})


class SuperDude {
    constructor(name, superPowers){
        this.name = name;
        this.powers(superPowers);
    }
    powers(){
      superPowers.forEach((power)=>{
        Object.defineProperty(this, power.name, {
            value: () => {;
              console.log(power.spell.apply(this));
            },
            writable: false,
            enumerable: false
          });
      })
    }
}

class Spell {
  constructor(name, spellFunc){
    this.name = name;
    this.spell(spellFunc);
  }
  spell(spellFunc){
    superPowers.push( {
      name: this.name,
      spell: function(){ return `${this.name} ${spellFunc}`}
    })
  }
}


let superBomb = new Spell('superBomb', 'will smash you');
superPowers[0].name = "sds"
superPowers[0].spell = "sds"
console.log(superPowers);


let Luther = new SuperDude('Luther', superPowers);

    // Тестирование: Методы должны работать и выводить сообщение.
    Luther.superSight();
    Luther.superSpeed();
    Luther.superFroze();
    Luther.Invisibility();
    Luther.superSkin();
    Luther.superBomb();
