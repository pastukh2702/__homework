/*

    Задание 1:
    Написать скрипт, который по клику на кнопку рандомит цвет и записывает его в localStorage
    После перезагрузки страницы, цвет должен сохранится.

*/

let button = document.getElementById('change');

const randomRGBNumber = () => {
    return Math.floor(Math.random() * 256);
}
  
const changeRGBToHEX = () => {
    let rgb = randomRGBNumber();
    let hex = rgb.toString(16);

    if (hex.length < 2) {
    hex = 0+hex;
    }

    return hex;
}
  
const collectHEXColor = () => {
    let RR = changeRGBToHEX();
    let GG = changeRGBToHEX();
    let BB = changeRGBToHEX();

    let color = '#'+RR+GG+BB;

    return color;
}

const storeColor = (name, data) => {
    localStorage.setItem(name, data);
}

const changeBg = () => {
    let color = collectHEXColor();
    document.body.style.background = color;
    storeColor('color', color);
}

const initColor = () => {
    let color = localStorage.getItem('color');
    document.body.style.background = color;
}
    window.addEventListener('load', initColor);
    button.addEventListener('click', changeBg);

