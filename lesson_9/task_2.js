/*

    Задание 2:
    Написать форму логина (логин пароль), которая после отправки данных записывает их в localStorage.
    Если в localStorage есть записть - На страниче вывести "Привет {username}", если нет - вывести окно
    логина.

    + бонус, сделать кнопку "выйти" которая удаляет запись из localStorage и снова показывает форму логина
    + бонус сделать проверку логина и пароля на конкретную запись. Т.е. залогинит пользователя если
    он введет только admin@example.com и пароль 12345678

*/
let lodinForm = document.getElementById('lodin-form');
let login = lodinForm.elements.login;
let pass = lodinForm.elements.pass;
let submit = lodinForm.querySelector('button');

let userWindow = document.getElementById('userWindow');

const initUser = () => {
    const exit = () => {
        localStorage.clear('user');
        location.reload();
    }
    let data = JSON.parse(localStorage.getItem('user'));
    if (data != null){
            lodinForm.style.display = 'none';
            userWindow.innerHTML = `Hello ${data.login}`;

        let exitButton = document.createElement('button');
            exitButton.innerHTML = 'logout';
            exitButton.addEventListener('click', exit);
            userWindow.appendChild(exitButton);   
    }
    
    
}

const enter = (e) => {
    const store = (user) => {
        localStorage.setItem('user', JSON.stringify(user))
    }
    let adminData = {
        login: 'admin@example.com',
        pass: 12345678
    }
    if( lodinForm.checkValidity() ){
        e.preventDefault();
        if(login.value == adminData.login && pass.value == adminData.pass){
            let user = {
                login: login.value,
                pass: pass.value
            }
            store(user);
            location.reload();
        }else{
            alert('uncorrect login or pass!')
        }
        
    }
}


window.addEventListener('load', initUser)
submit.addEventListener('click', enter);