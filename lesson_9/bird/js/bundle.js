/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./task_4.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./bird.js":
/*!*****************!*\
  !*** ./bird.js ***!
  \*****************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _modules_import__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modules/import */ \"./modules/import.js\");\n\n\nclass Bird{\n    constructor(name, func){\n        this.name = name\n        this.render();\n        Object(_modules_import__WEBPACK_IMPORTED_MODULE_0__[\"default\"])(this, func);\n    }\n    render(){\n        console.log(this)\n    }\n\n  \n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Bird);\n\n//# sourceURL=webpack:///./bird.js?");

/***/ }),

/***/ "./modules/eat.js":
/*!************************!*\
  !*** ./modules/eat.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst eat = (t) => {\n    console.log(t.name, 'eat')\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (eat);\n\n//# sourceURL=webpack:///./modules/eat.js?");

/***/ }),

/***/ "./modules/import.js":
/*!***************************!*\
  !*** ./modules/import.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _eat__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./eat */ \"./modules/eat.js\");\n\n\nconst doS = (t, func) => {\n    switch (func) {\n        case 'eat':\n        Object(_eat__WEBPACK_IMPORTED_MODULE_0__[\"default\"])(t);\n            break;\n    \n        default:\n            break;\n    }\n    \n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (doS);\n\n//# sourceURL=webpack:///./modules/import.js?");

/***/ }),

/***/ "./task_4.js":
/*!*******************!*\
  !*** ./task_4.js ***!
  \*******************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _bird__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bird */ \"./bird.js\");\n\n/*\n\n  Задание:\n    Написать конструктор обьекта. Отдельные функции разбить на модули\n    и использовать внутри самого конструктора.\n    Каждую функцию выделить в отдельный модуль и собрать.\n\n    Тематика - птицы.\n    Птицы могут:\n      - Нестись\n      - Летать\n      - Плавать\n      - Кушать\n      - Охотиться\n      - Петь\n      - Переносить почту\n      - Бегать\n\n  Составить птиц (пару на выбор, не обязательно всех):\n      Страус\n      Голубь\n      Курица\n      Пингвин\n      Чайка\n      Ястреб\n      Сова\n\n */\n\n \n\n new _bird__WEBPACK_IMPORTED_MODULE_0__[\"default\"]('vova', 'eat');\n\n//# sourceURL=webpack:///./task_4.js?");

/***/ })

/******/ });