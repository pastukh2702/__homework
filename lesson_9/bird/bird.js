import doS from './modules/import';

class Bird{
    constructor(name, func){
        this.name = name
        this.render();
        doS(this, func);
    }
    render(){
        console.log(this)
    }

  
}

export default Bird;