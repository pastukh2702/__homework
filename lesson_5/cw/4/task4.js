/*

  Задание "Шифр цезаря":

    https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

    Написать функцию, которая будет принимать в себя слово и количество
    симовлов на которые нужно сделать сдвиг внутри.

    Написать функцию дешефратор которая вернет слово в изначальный вид.

    Сделать статичные функции используя bind и метод частичного
    вызова функции (каррирования), которая будет создавать и дешефровать
    слова с статично забитым шагом от одного до 5.


    const isMobile = document.innerWidth < 768;

    Например:
      encryptCesar( 3, 'Word');
      encryptCesar1(...)
      ...
      encryptCesar5(...)

      decryptCesar1(3, 'Sdwq');
      decryptCesar1(...)
      ...
      decryptCesar5(...)

      "а".charCodeAt(); // 1072
      String.fromCharCode(189, 43, 190, 61) // ½+¾

*/

function encryptFunc(key , word){
  word = Array.from(word);
  let encword = '';
  word.forEach(function(e){
    encword += String.fromCharCode(e.charCodeAt()+key);
  });
  console.log(encword);
}

function decryptFunc(key , word){
  word = Array.from(word);
  let decword = '';
  word.forEach(function(e){
    decword += String.fromCharCode(e.charCodeAt()-key);
  });
  console.log(decword);
}

let encryptCesar1 = encryptFunc.bind(null, 1);
let encryptCesar2 = encryptFunc.bind(null, 2);
let encryptCesar3 = encryptFunc.bind(null, 3);
let encryptCesar4 = encryptFunc.bind(null, 4);
let encryptCesar5 = encryptFunc.bind(null, 5);

let decryptCesar1 = decryptFunc.bind(null, 1);
let decryptCesar2 = decryptFunc.bind(null, 2);
let decryptCesar3 = decryptFunc.bind(null, 3);
let decryptCesar4 = decryptFunc.bind(null, 4);
let decryptCesar5 = decryptFunc.bind(null, 5);

