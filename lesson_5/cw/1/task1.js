/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х

*/

var Train = {
    name: "JS EXPRESS",
    speed: 130,
    pass: 20,

    go: function(){
        console.log("Поезд "+this.name+" везет "+this.pass+" человек со скоростью "+this.speed)
    },
    idle: function(){
        console.log("Поезд "+this.name+" остановился. "+"Скорость "+(this.speed - this.speed));
    },
    getPass: function(x){
        console.log("Поезд "+this.name+" подобрал пассажиров. Количество пассажиров - "+ (this.pass+=x));
    }
}

Train.go();
Train.idle();
Train.getPass(10);