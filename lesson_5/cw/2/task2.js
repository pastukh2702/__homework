/*

    Задание 2.

    Напишите фунцию, которая изменяет цвет-фона и цвет текста, присваивая к новым цветам
    значение из this.color, this.background
    А так же добавляет элемент h1 с текстом "I know how binding works in JS"

    1.1 Ф-я принимает один аргумент,
    второй попадает в него через метод .call(obj)

    1.2 Ф-я не принимает никаких аргументов,
    а необходимые настройки полностью передаются через bind

    1.3 Ф-я принимает фразу для заголовка,
    обьект с настройками передаем через .apply();

*/
  let colors = {
    background: 'purple',
    color: 'white',

    word: 'I know how binding works in JS'
  }

  // 1 Ф-я
  function myCall( color ){
    document.body.style.background = this.background;
    document.body.style.color = color;
  }
  myCall.call( colors, 'red' );

  // 2 Ф-я
  function myCall2(){
    document.body.style.background = this.background;
    document.body.style.color = this.color;
  }
  let myCall2Bind = myCall2.bind( colors );
  myCall2Bind();

  // 3 Ф-я
  function myCall3(){
    let word = document.querySelector('h1');
        word.innerHTML = this.word;
  }
  myCall3.apply( colors );

