/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>


*/
let CommentsArray = [];

let commentOpt = {
  avatarUrl: 'https://pitomec.guru/wp-content/themes/pitomec/assets/img/avatar__user.jpg',
  addLike: function(e){ 
    this.likes++;
    e.target.innerHTML = this.likes + ' like';
  }
}

function Comment( name, text, avatarUrl){
    this.name = name;
    this.text = text;
    this.likes = 0;
    if( avatarUrl !== undefined){
      this.avatarUrl = avatarUrl;
    }

    CommentsArray.push(this)
    Object.setPrototypeOf( this, commentOpt );
}


let myComment1 = new Comment('Володя','Боже как круто! Я смогу накликать себе 1000 лайков!'); 
let myComment2 = new Comment('Андрюша Петренко','Ребята а как тут лайк поставить???','https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSmGfp-ig4XCUM90-72ZZEf2Pe9IOhwZnzbu3QXU6SJUGc3vp-n');
let myComment3 = new Comment('Cowboy2702','Андрюша, спроси у Володи');
let myComment4 = new Comment('САНЯ222','Йоу, посмотрите на мою крутую аватарку!','http://img.1001mem.ru/posts_temp/16-10-26/3868488.jpg');


function commentRender(){
  let arr = CommentsArray;
  let CommentsFeed = document.getElementById('CommentsFeed');

  arr.forEach(function(e){
    let addLike = commentOpt.addLike.bind(e);

    let comment = document.createElement('div');
        comment.className = 'comment';
    
    let like = document.createElement('div');
        like.innerHTML = e.likes + ' like';
        like.className = 'like';
        like.addEventListener('click', addLike);
        
    let avatar = document.createElement('img');
        avatar.className = 'avatar';
        avatar.src = e.avatarUrl;

    let name = document.createElement('h3');
        name.className = 'name';
        name.innerHTML = e.name;

    let text = document.createElement('span');
        text.className = 'text';
        text.innerHTML = e.text;

        comment.appendChild(like);
        comment.appendChild(avatar);
        comment.appendChild(name);
        comment.appendChild(text);

        CommentsFeed.appendChild(comment);
  })
}

commentRender();












