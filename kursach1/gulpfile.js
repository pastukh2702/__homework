const gulp = require('gulp');
const less = require('gulp-less');
const concat = require('gulp-concat');
const del = require('del');
const newer = require('gulp-newer');
const debug = require('gulp-debug');
const browserSync = require('browser-sync').create();

gulp.task('clean', function(){
    return del(['public/']);
});
gulp.task('styles', function(){
    return gulp.src('dev/less/*.less')
      .pipe( less({
        env: 'production'
      }) )
      .pipe( concat('style.min.css') )
      .pipe( gulp.dest('public/css') );
});
gulp.task('assets', function() {
    return gulp.src('dev/assets/**')
      .pipe( newer('public') )
      .pipe( debug() )
      .pipe( gulp.dest('public') );
});


gulp.task('build', gulp.series('clean', 'styles', 'assets'));
gulp.task('watch', function() {
    gulp.watch( 'dev/less/*.less', gulp.series( 'styles' ) );
    gulp.watch( 'dev/less/**/*.less', gulp.series( 'styles' ) );
    gulp.watch( 'dev/assets/**/*.*', gulp.series( 'assets' ) );
});
gulp.task('serve', function(){
  browserSync.init({
    server: "public"
  });
  browserSync.watch('public/**/*.*').on('change', browserSync.reload );
});

gulp.task('dev',
  gulp.series(
    'build',
    gulp.parallel('watch', 'serve')
  )
);
