// Імпорт всіх модулів
import adminPanelInit from './modules/adminpanel/import';
import renderInit from './modules/homepage/render';
import filter from './modules/filter/filter';
import renderFilter from './modules/filter/render';
import menu from './modules/menu/menu';

// Ініціювання всіх модулів через перехід на іншу сторінку
if (window.location.pathname === '/admin.html'){
    adminPanelInit();
}
window.addEventListener('popstate', e => {
    if(e.state.page == "home"){
        renderInit('all');
    }
    if(e.state.page == "kitchen"){
        renderInit(e.state.company);
    }
});
// Ініціювання всіх модулів
renderInit('all');
renderFilter();
filter();
menu();























