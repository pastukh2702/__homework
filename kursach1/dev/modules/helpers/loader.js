let loaderContainer = document.querySelector('.loader');

const loader = () => {
    loaderContainer.style.height = '100%';
    document.body.style.overflow = 'hidden';
    document.querySelector('img').addEventListener('load', () => {
        setTimeout(()=>{ 
            loaderContainer.style.height = 0;
            document.body.style.overflow = 'auto';
        }, 2000)

    })
}

export default loader;
