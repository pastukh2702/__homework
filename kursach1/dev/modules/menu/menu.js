import openPage from '../aboutpage/openPage';
let menuBtn = document.querySelector('.menu');
let menuContainer = document.querySelector('.menu-container');
let open = false;


const menu = () => {
    menuBtn.addEventListener('click', () => {
        if(open === false){
            open = true;
            menuContainer.style.transform = 'translateY(0em)';
        }else{
            open = false;
            menuContainer.style.transform = 'translateY(-28em)';
        }
    })
    menuContainer.querySelector('.about').addEventListener('click', openPage)
}

export default menu;