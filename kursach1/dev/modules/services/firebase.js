// Підключення бібліотек для роботи з базою даних
let firebase = 
    require('firebase/app')
    require('firebase/auth')
    require('firebase/storage')
    require('firebase/firestore')

// Конфігурація підключення
let firebaseConfig = {
    apiKey: "AIzaSyAKIjOAJI8VXYwBoA9027QXz5ThX9xrz6o",
    authDomain: "kitchen-4d23e.firebaseapp.com",
    databaseURL: "https://kitchen-4d23e.firebaseio.com",
    projectId: "kitchen-4d23e",
    storageBucket: "kitchen-4d23e.appspot.com",
    messagingSenderId: "279882870507",
    appId: "1:279882870507:web:50bd64a9d314c363"
};

// Ініціювання підключення
firebase.initializeApp(firebaseConfig);

// Ініціювання бібліотек
const auth = firebase.auth();
const db = firebase.firestore();
const storage = firebase.storage();

export {auth, db, storage};