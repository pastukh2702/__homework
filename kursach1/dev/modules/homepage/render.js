// Підключення бази даних services
import * as fire from '../services/firebase';
// Підключення loader
import loader from '../helpers/loader';
// Підключення сторінки кухні
import * as kitchenPage from '../kitchenpage/import'

// Отримання значень фільтру
const renderInit = (company) => {
    history.pushState({page: 'home'}, 'kitchen+ home', '/');

    if(company != 'all'){
        fire.db.collection('kitchens').where('company','==',company).get().then(snapshot => {
            renderKitchen(snapshot.docs);
        })
    }else{
        fire.db.collection('kitchens').get().then(snapshot => {
            renderKitchen(snapshot.docs);
        })
    } 
}
// Функція відрисовки
const renderKitchen = (data) => {
    let kitchensContainer = document.querySelector('.kitchens-container');
        kitchensContainer.innerHTML = '';
        kitchensContainer.style = 'grid-template-columns: repeat(auto-fill, minmax(30rem, 1fr));';
        
    data.forEach((doc, index) => {
        if(doc.data().photos != undefined){
            let block = document.createElement('div');
            block.classList.add('block');
            block.innerHTML = `
                <img src=${doc.data().photos[0]} alt="">
                <div class="title-container">
                    <h3>${doc.data().name} ${doc.data().company} <span>-$${doc.data().price}</span></h3>
                </div>
            `
            block.addEventListener('click', kitchenPage.openPage(doc))
            kitchensContainer.appendChild(block)
        }
    });
    // Запуск лоадера при ініціювання сторінки
    loader();
}

export default renderInit;