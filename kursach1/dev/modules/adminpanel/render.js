import * as fire from '../services/firebase';
import deleteKitchen from './delete';

const render = () => {
    fire.auth.onAuthStateChanged(user => {
        if(user){
            fire.db.collection('kitchens').get().then(snapshot => {
                renderKitchens(snapshot.docs)
            })
            login.style.display = 'none';
            logout.style.display = 'block';
            create.style.display = 'block';
        }else{
            renderKitchens([]);
            console.log('Login First!');
            login.style.display = 'block';
            logout.style.display = 'none';
            create.style.display = 'none';
        }
    })
}

const renderKitchens = (data) => {
    let kitchens = document.querySelector('#kitchens');
        kitchens.innerHTML = '';
    data.forEach(doc => {
        let kitchen = document.createElement('div');
            kitchen.classList.add('kitchen');
            kitchen.innerHTML = `
                <h5>Kitchen name: <br><span>${doc.data().name}</span></h5>
                <h5>Company name: <br><span>${doc.data().company}</span></h5>
                <h5>Price: <br><span>${doc.data().price}</span></h5>
                <div class="materials">
                </div>
                <span><a id="delete">delete</a></span>
            `

            doc.data().photos.forEach((photo, index) => {
                let img = document.createElement('img');
                    img.src = photo;
                let title = document.createElement('h3');
                    title.innerHTML = doc.data().titles[index];
                let about = document.createElement('p');
                    about.innerHTML = doc.data().abouts[index];

                    kitchen.querySelector('.materials').appendChild(img)
                    kitchen.querySelector('.materials').appendChild(title)
                    kitchen.querySelector('.materials').appendChild(about);
            })
            kitchen.querySelector('#delete').addEventListener('click', deleteKitchen(doc.id, doc.data().company, doc.data().name, doc.data().filesNames));
            kitchen.querySelector('#delete').style.background = "red"
            kitchens.appendChild(kitchen);
    })
}

export default render;