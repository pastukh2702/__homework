// Підключення бази даних services
import * as fire from '../services/firebase'; 
// Підключення функції відрисовки вікна входу
import * as modal from './modal';

// Поля вводу для входу
const login = document.querySelector('#login');
const logout = document.querySelector('#logout');

// Функція ініціювання
const init = () => {
    logout.addEventListener('click', (e) => {
        fire.auth.signOut();
        location.reload();
    })
    
    login.addEventListener('click', (e) => {
        modal.modalOpen(e.target.dataset.modal);
    
        let loginForm = document.querySelector('#login-form');
            loginForm.addEventListener('submit', (e) => {
                e.preventDefault();
    
                const email = loginForm['login-email'].value;
                const password = loginForm['login-password'].value;
    
                fire.auth.signInWithEmailAndPassword(email, password).then(cred => {
                    modal.modalReset();
                })
            })     
    })
}

// Експорт функції
export {init};