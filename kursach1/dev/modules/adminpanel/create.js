// Підключення функції відрисовки вікна створення кухні
import * as modal from './modal';
// Підключення бази даних services
import * as fire from '../services/firebase';

const create = document.querySelector('#create');

// Функція ініціювання
const init = () => {
    create.addEventListener('click', (e) => {
        e.preventDefault();
        modal.modalOpen(e.target.dataset.modal);
    
        let createForm = document.querySelector('#create-form');
            createForm.addEventListener('submit', (e) => {
                e.preventDefault();
    
                // Поля вводу інформації для створення кухні
                const company = createForm['create-company'].value;
                const price = createForm['create-price'].value;
                const name = createForm['create-name'].value;
                const files = createForm.querySelectorAll('input[type=file]');
                const textareas = createForm.querySelectorAll('textarea');
                const titles = createForm.querySelectorAll('.title');
                const filesLinks = [];
                const filesNames = [];
                const textareasText = [];
                const titlesText = [];
                let id;
                    fire.db.collection('kitchens').add({
                        company: company,
                        name: name,
                        price: price
                    }).then(res => {
                        id = res.id;
                        modal.modalReset();
                    })
                    files.forEach((item, index) => {
                        let file = item.files[0];
                        let textarea = textareas[index].value;
                        let title = titles[index].value;
                        let photoRef = fire.storage.ref(`kitchens/${company}/${name}/`+file.name);
                        // Функція відправки медіа файлів до сервера
                        photoRef.put(file)
                        .then(() => {
                            let storeRef = fire.storage.ref(`/kitchens/${company}/${name}`)
                                storeRef.child(file.name).getDownloadURL().then(getUrl => {
                                    filesLinks.push(getUrl);
                                    filesNames.push(file.name)
                                    textareasText.push(textarea);
                                    titlesText.push(title);

                                    // Додавання посилання на медіа файли до бази даних
                                    fire.db.collection('kitchens').doc(id).set({
                                        photos: filesLinks,
                                        filesNames: filesNames,
                                        titles: titlesText,
                                        abouts: textareasText
                                    }, { merge: true })
                                    alert('photo 1 of:',item.files[index],'loaded. Please watit...')
                                })
                        })
                    })  
                    
            })   
    })
}

// Експорт функції
export {init};