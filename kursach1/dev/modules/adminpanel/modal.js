const modalOpen = (modalName) => {
    let modal = document.querySelector(`#${modalName}`);
        modal.style.display = 'flex';
        modal.addEventListener('click', (e) => {
            if(e.target.id == modalName){
                modalReset();
            }
        })
}
const modalReset = () => {
    let modals = document.querySelectorAll('.modal');
        modals.forEach((modal) => {
            modal.querySelector('form').reset()
            modal.style.display = 'none';
        })
        
}

const addFileField = (e) => {
    e.preventDefault();
    let files = document.querySelector('#files');
    let fileField = document.createElement('div');
        fileField.classList.add('file-field');

    let textareaField = document.createElement('div');
        textareaField.classList.add('input-field');

    let titleField = document.createElement('div');
        titleField.classList.add('input-field');
        
        

        fileField.innerHTML = `<button>remove</button>`;
        fileField.querySelector('button').addEventListener('click', () => {
            fileField.remove();
            textareaField.remove();
            titleField.remove();
        })
    let file = document.createElement('input');
        file.type = 'file';
    let textarea = document.createElement('textarea');
        textarea.cols = 10;
        textarea.rows = 10;
    let title = document.createElement('input');
        title.classList.add('title');
        
    

        titleField.appendChild(title);
        textareaField.appendChild(textarea);
        fileField.appendChild(file);
        files.appendChild(fileField)
        files.appendChild(titleField);
        files.appendChild(textareaField)
        console.log(files.querySelectorAll('textarea')[0].value)
        
}
const init = () => {
    let addFieldBtn = document.querySelector('#add-field');
        addFieldBtn.addEventListener('click', addFileField)
}


export {modalOpen, modalReset, init}