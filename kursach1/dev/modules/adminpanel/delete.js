// Підключення бази даних services
import * as fire from '../services/firebase';
// Підключення відрисовки всіх кухонь з бази даних
import renderKitchens from './render'

const deleteKitchen = (id ,company, name, filesNames) => (e) => {
    let storeRef = fire.storage.ref(`/kitchens/${company}/${name}`);
    filesNames.forEach((file) => {
        storeRef.child(file).delete()
    })

    fire.db.collection('kitchens').doc(id).delete().then(() => {
        fire.db.collection('kitchens').get().then(snapshot => {
            renderKitchens(snapshot.docs)
        })
    })
}
// Експорт функції
export default deleteKitchen;