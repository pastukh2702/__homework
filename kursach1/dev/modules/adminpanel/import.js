import * as auth from './auth';
import render from './render';
import * as create from './create';
import * as modal from './modal';

const adminPanelInit = () => {
    auth.init();
    render();
    create.init();
    modal.init();
}

export default adminPanelInit;

