let pageCounter = 0;

const kitchenPage = (photo, data, index, arr) => {
        
        

        if(pageCounter == 0){
                let page = document.createElement('div');
                page.classList.add('kitchen-photo');
                page.innerHTML = `
                        <img src=${photo}>
                `
                pageCounter = 1;
                return page
        }
        if(pageCounter == 1){
                let page = document.createElement('div');
                page.classList.add('kitchen-text')
                page.innerHTML = `
                        <h2>${data.data().titles[index]}</h2>
                        <p>${data.data().abouts[index]}</p>
                `
                pageCounter = 2;
                return page
        }
        if(pageCounter == 2){
                let page = document.createElement('div');
                page.classList.add('kitchen-text')
                page.innerHTML = `
                        <h2>${data.data().titles[index]}</h2>
                        <p>${data.data().abouts[index]}</p>
                `
                pageCounter = 3;
                return page
        }
        if(pageCounter == 3){
                let page = document.createElement('div');
                page.classList.add('kitchen-photo');
                page.innerHTML = `
                        <img src=${photo}>
                `
                pageCounter = 0;
                return page
        }

        if(arr.length-2 == index && pageCounter != 10){
                let page = document.createElement('div');
                page.classList.add('kitchen-form');
                page.innerHTML = `
                        <form action="https://formspree.io/vovaka2702@gmail.com" method="POST">
                                <h2>Get consulting now</h2>
                                <label for="form-name">Name</label>
                                <input name="Name" type="text" id="form-name">
                                <label for="form-surname">Surname</label>
                                <input name="Surname" type="text" id="form-surname">
                                <label for="form-telephone">Telephone</label>
                                <input name="Telephone" type="text" id="form-telephone">
                                <button>Send</button>
                        </form>
                `
                pageCounter = 10;
                return page
        }
        if(pageCounter == 10){
                let page = document.createElement('div');
                page.classList.add('kitchen-photo');
                page.innerHTML = `
                        
                        <a href="https://goo.gl/maps/pTwiSgekQjka9nSP6" target="_blank"><img src="../img/map.png"></a>   
                    
                `
                pageCounter = 0;
                return page
        }

                
        
       
        
        
}

export default kitchenPage;

