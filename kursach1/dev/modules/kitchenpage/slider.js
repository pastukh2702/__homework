
let currentSlide = 0;
const slider = (data) => {
    let sliderContainer = document.createElement('div');
        sliderContainer.classList.add('slider')
    let sliderImages = document.createElement('div');
        sliderImages.classList.add('slider-images');
    
    let sliderControls = document.createElement('div');
        sliderControls.classList.add('slider-controls');

    let next = document.createElement('span');
        next.innerHTML = '>';
    let prev = document.createElement('span');
        prev.innerHTML = '<';

        sliderControls.appendChild(prev);
        sliderControls.appendChild(next);

        sliderContainer.appendChild(sliderImages);
        sliderContainer.appendChild(sliderControls);

        data.forEach((photo, index) => {
            sliderImages.innerHTML+= `
            <img src=${photo} data-numer=${index}>
            `
        })

        next.addEventListener('click', () => {
            if(currentSlide > data.length - 2){
                currentSlide = 0;
                render(sliderImages);
            }else{
                currentSlide++;
                render(sliderImages);
            }
        })

        prev.addEventListener('click', () => {
            if(currentSlide < 1){
                currentSlide = data.length-1;
                render(sliderImages);
            }else{
                currentSlide--;
                render(sliderImages);
            }
        })

        
        sliderImages.querySelector('img[data-numer="0"]').style.display = "block";
        
        return sliderContainer;
}

const render = (images) => {
    images.querySelectorAll('img').forEach((image) => {
        if(image.dataset.numer == currentSlide){
            image.style.display = 'block';
        }else{
            image.style.display = 'none';
        }
    })

}

export default slider;