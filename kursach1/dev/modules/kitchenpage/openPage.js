import loader from '../helpers/loader';
import slider from './slider';
import kitchenPage from './page';

const openPage = (data) => (e) => {
    // history.pushState({page: 'kitchen',company: data.data().company}, `kitchen+ ${data.data().company}`, `/${data.data().company}/${data.data().name}`);
        window.scrollTo(0,0);
    let kitchensContainer = document.querySelector('.kitchens-container');
        kitchensContainer.innerHTML = ''
        kitchensContainer.style = 'grid-template-columns: repeat(auto-fill, minmax(40rem, 1fr));';
        
        data.data().photos.forEach((photo, index, arr)=>{
            kitchensContainer.appendChild(kitchenPage(photo, data, index, arr));
            kitchensContainer.appendChild(kitchenPage(photo, data, index, arr));
        })
        kitchensContainer.appendChild(contactBlock());
        kitchensContainer.appendChild(mapBlock());
        loader()
}

const contactBlock = () => {
    let contact = document.createElement('div');
        contact.classList.add('kitchen-form');
        contact.innerHTML = `
                <form action="https://formspree.io/vovaka2702@gmail.com" method="POST">
                        <h2>Get consulting now</h2>
                        <label for="form-name">Name</label>
                        <input name="Name" type="text" id="form-name">
                        <label for="form-surname">Surname</label>
                        <input name="Surname" type="text" id="form-surname">
                        <label for="form-telephone">Telephone</label>
                        <input name="Telephone" type="text" id="form-telephone">
                        <button>Send</button>
                </form>
        `
        return contact
}
const mapBlock = () => {
    let map = document.createElement('div');
        map.classList.add('kitchen-photo');
        map.innerHTML = `
                
                <a href="https://goo.gl/maps/pTwiSgekQjka9nSP6" target="_blank"><img src="../img/map.png"></a>   
            
        `
        return map
}


export default openPage;