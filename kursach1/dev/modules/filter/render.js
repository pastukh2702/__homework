// Підключення бази даних services
import * as fire from '../services/firebase';

// Додавання до списку назви компанії, взятої з бази
const renderFilter = () => {
    fire.db.collection('kitchens').get().then(snapshot => {
        renderCompanySelect(snapshot.docs);
    })
}

// Вибір компанії та відправка на відмальовку
const renderCompanySelect = (data, company) => {
    let companySelect = document.querySelector('#company-select');
        companySelect.options.length = 0;
        companySelect.options[companySelect.options.length] = new Option('All company', 'all');
    let filterData = [];
        data.forEach((doc, index) => {
            if(doc.data().photos != undefined){
                filterData.push(doc.data().company);
            }
            
            
        });

        deleteSome(filterData).forEach((filter) => {
            companySelect.options[companySelect.options.length] = new Option(filter, filter);
        })
}

const deleteSome = (arr) => {
    let obj = {};

    for (var i = 0; i < arr.length; i++) {
      var str = arr[i];
      obj[str] = true; // запомнить строку в виде свойства объекта
    }
  
    return Object.keys(obj);
}
// Експорт функції
export default renderFilter;