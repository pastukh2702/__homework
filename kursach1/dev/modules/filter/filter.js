import renderInit from '../homepage/render';

let companySelect = document.querySelector('#company-select');
let filterBtn = document.querySelector('.filter');
let filterContainer = document.querySelector('.filter-container');
let open = false;

const filter = () => {
    filterBtn.addEventListener('click', () => {
        if(open === false){
            open = true;
            filterContainer.style.transform = 'translateY(0em)';
        }else{
            open = false;
            filterContainer.style.transform = 'translateY(-14em)';
        }
    })
    companySelect.addEventListener('change', () => {
        let company = companySelect.options[companySelect.selectedIndex].value;
        if(company == undefined){
            renderInit();
        }else{
            renderInit(company);
        }
    })
}

export default filter;