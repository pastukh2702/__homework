import * as fire from '../services/firebase';
import page from './page';
import loader from '../helpers/loader';

const openPage = () => {
    fire.db.collection('abouts').doc('about').get().then(snapshot => {
        let kitchensContainer = document.querySelector('.kitchens-container');
        kitchensContainer.innerHTML = ''
        kitchensContainer.style = 'grid-template-columns: repeat(auto-fill, minmax(40rem, 1fr));';

        snapshot.data().text.forEach((doc, index, arr)=>{
            kitchensContainer.appendChild(page(snapshot, index, arr));
            kitchensContainer.appendChild(page(snapshot, index, arr));
        })
        loader()
    })
    
}

export default openPage;