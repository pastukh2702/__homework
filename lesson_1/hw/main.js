/*

  Задание 1.

  Написать функцию которая будет задавать СЛУЧАЙНЫЙ цвет для фона.
  Каждая перезагрузка страницы будет с новым цветом.
  Для написания используйте функцию на получение случайного целого числа,
  между минимальным и максимальным значением (Приложена снизу задания)

  + Бонус, повесить обработчик на кнопку через метод onClick
  + Бонус, использовать 16-ричную систему исчесления и цвет HEX -> #FFCC00
  + Бонус выводить полученый цвет по центру страницы.
  
  Необходимо создать блок через createElement задать ему стили через element.style
  и вывести через appendChild или insertBefore

  Необходимые материалы:
    Math.Random (Доки): https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Math/random
    function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    __
    Работа с цветом:
    Вариант 1.
      Исользовать element.style.background = 'rgb(r,g,b)';
      где r,g,b случайное число от 0 до 255;

    Вариант 2.
      Исользовать element.style.background = '#RRGGBB';
      где, RR, GG, BB, значние цвета в 16-ричной системе исчесления
      Формирование цвета в вебе: https://ru.wikipedia.org/wiki/%D0%A6%D0%B2%D0%B5%D1%82%D0%B0_HTML
      Перевод в 16-ричную систему исчесления делается при помощи
      метода Number.toString( 16 ) https://www.w3schools.com/jsref/jsref_tostring_number.asp,

      var myNumber = '251'
      myNumber.toString(16) // fb

*/
document.addEventListener('DOMContentLoaded', function(){ //AUTO FUNC !!!
    var h = document.createElement('h1');
        h.id = 'displayColor';

    var div = document.createElement('div');
        div.id = 'rollerButton';  //use css for style
         div.onclick = function(){  //onclick function 
        changeBgColor();
    }
    var app = document.getElementById('app');
        app.appendChild(h);
        app.appendChild(div);
})



function randomRGBNumber(){
  return Math.floor(Math.random() * 256);
}

function changeRGBToHEX(){
  var rgb = randomRGBNumber();
  var hex = rgb.toString(16);

  if (hex.length < 2) {
    hex = 0+hex;
  }

  return hex;
}


function collectHEXColor(){
  var RR = changeRGBToHEX();
  var GG = changeRGBToHEX();
  var BB = changeRGBToHEX();

  var color = '#'+RR+GG+BB;

  return color;
}


function displayNameBgColor(){
  var color = collectHEXColor();
  var h = document.getElementById('displayColor')
      h.innerHTML = color.toUpperCase();

  console.clear();
  console.log("%cYOUR COLOR HERE > "+color.toUpperCase()+"", "font-size:20px; color: "+color+"");

  return color;
}

function changeBgColor(){
  var color = displayNameBgColor();
  document.body.style.background = color;
}

changeBgColor();






