import renderComment from './render';

class Comment{
    constructor(name, message){
        this.name = name;
        this.message = message;
        this.date = this.date();
        this.store();
    }
    store(){
        console.log(this);
    }
    date(){
        let date = new Date();
        let dd = date.getDate();
        if (dd < 10) dd = '0' + dd;

        let mm = date.getMonth() + 1;
        if (mm < 10) mm = '0' + mm;

        let yy = date.getFullYear() % 100;
        if (yy < 10) yy = '0' + yy;

        let hh = date.getHours();
        if (hh < 10) hh = '0' + hh;

        let mn = date.getMinutes();
        if (mn < 10) mn = '0' + mn;

        return `${dd}/${mm}/${yy} ${hh}:${mn}`;
    }

}

export default Comment;