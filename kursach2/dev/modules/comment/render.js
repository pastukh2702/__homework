import * as helpers from '../helpers/import';

const renderComment = (post) => () =>{
    let data = helpers.Data;
    let getData = data.getData();

    getData.forEach((el) => {
        if (el.id == post.dataset.id){
            let comments = post.querySelector('.comments');
            if(comments.querySelectorAll('.comment')){
                comments.querySelectorAll('.comment').forEach((el)=>{
                    el.remove();
                })
            }
            el.comments.forEach((comm) => {
                
                let comment = document.createElement('div');
                comment.classList.add('comment');
                comment.innerHTML = `
                    <span class="body1 comment-name">${comm.name}</span>
                    <p class="body2 comment-text">${comm.message}</p>
                    <p class="caption">${comm.date}</p>
                `
                comments.appendChild(comment)
            })
            
        }
    })
}

export default renderComment;