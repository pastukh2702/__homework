import Post from '../post/post';
import render from '../post/render';

let open = false;

let popup = document.querySelector('.popup_post');
let popupBg = popup.querySelector('.popup_bg');
let popupCt = popup.querySelector('.popup_ct');
let popupButton = popupCt.querySelector('button');

let postForm = document.getElementById('post-form');



const sendPost = (e) => {
    let author = document.getElementById('post-author').value;
    let image = document.getElementById('post-image').value;
    let title = document.getElementById('post-title').value;
    let text = document.getElementById('post-text').value;

    if(postForm.checkValidity()){
        e.preventDefault();
        new Post(author, title, text, image);
        location.reload();
    }
}

const postButton = () => {
    if ( open == false) {
        popup.style = 'display: flex;'
        popupBg.style = 'display: flex;'
        open = true;
        document.body.style = "overflow: hidden;"
        popupBg.addEventListener('click', () => {
            popup.style = 'display: none;';
            popupBg.style = 'display: none;'
            document.body.style = "overflow: auto;"
            open = false;
            popupButton.removeEventListener('click', sendPost);
        })
        popupButton.addEventListener('click', sendPost);
    }
}

export default postButton;