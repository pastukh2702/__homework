import * as helpers from '../helpers/import';

let data = helpers.Data;
let posts = [];


class Post{
    constructor( author, title, text, image ){
        this.author = author;
        this.title = title;
        this.text = text;
        this.likes = 0;
        this.commentsCounter = 0;
        this.comments = [];
        this.id = new Date().getTime();
        this.date = this.date();

        if (image !== '') {
            this.image = image;
        }else{
            this.image = 'http://www.flyml.net/wp-content/uploads/2018/06/none_img.gif';
        }

        posts.push(this);
        this.store();
        
    }

    store(){
        let getData = data.getData();

        if(getData !== null){
            getData.unshift(this);
            data.setData(getData);
        }else{
            data.setData(posts);
        }
    }
    date(){
        let date = new Date();
        let dd = date.getDate();
        if (dd < 10) dd = '0' + dd;

        let mm = date.getMonth() + 1;
        if (mm < 10) mm = '0' + mm;

        let yy = date.getFullYear() % 100;
        if (yy < 10) yy = '0' + yy;

        let hh = date.getHours();
        if (hh < 10) hh = '0' + hh;

        let mn = date.getMinutes();
        if (mn < 10) mn = '0' + mn;

        return `${dd}/${mm}/${yy} ${hh}:${mn}`;
    }
}

export default Post;
