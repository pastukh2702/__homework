import Post from './post';
import renderPost from './render';

export {Post, renderPost};