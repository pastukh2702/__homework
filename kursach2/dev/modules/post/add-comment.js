import Comment from '../comment/comment';
import renderComment from '../comment/render';
import * as helpers from '../helpers/import';

let open = false;

let data = helpers.Data;
let getData = data.getData();

let popup = document.querySelector('.popup_comment');
let popup_ct = popup.querySelector('.popup_ct');
let popupBg = document.querySelector('.popup_bg');
let popupButton = popup_ct.querySelector('button');

let commentForm = document.getElementById('comment-form');

const sendComment = (post) => (e) => {
    let name = document.getElementById('name-input');
    let message = document.getElementById('message-input');

    if(commentForm.checkValidity()){
        e.preventDefault();
        getData.forEach((el) => {
            if (el.id == post.dataset.id){
                el.comments.unshift(new Comment(name.value, message.value));
                el.commentsCounter += 1;
                data.setData(getData);
                setTimeout(renderComment(post),100); //wait localstore))))
            }
        })
    }

}

const addComment = (post) => (e) => {
    if(open == false){
        popup.style = `display: flex;`;
        popupBg.style = 'display: flex;';
        window.scrollTo({
            top: post.offsetTop-60,
            behavior: "smooth"
        });
        
        popupButton.addEventListener('click', sendComment(post));
        post.appendChild(popup);
        

        popupBg.addEventListener('click', () => {
            open = false;
            popup.style = 'display: none;';
            popupBg.style = 'display: none;';
            popupButton.removeEventListener('click', sendComment);
            location.reload();
        })
    }
}

export default addComment;