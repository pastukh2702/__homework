import addLike from './add-like';
import addComment from './add-comment';
import openImage from './open-image';
import renderComment from '../comment/render';
import * as helpers from '../helpers/import';

let data = helpers.Data;
let getData = data.getData();

const sizePost = () => {
    document.querySelectorAll('img').forEach((img) => {
        img.addEventListener('load', resizePost);
    })
}

const resizePost = () => {
    let posts = document.querySelectorAll('.post');
        posts.forEach((post) => {
            let grid = document.querySelector('.posts');
            let rowHeight = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-auto-rows'));
            let rowGap = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-row-gap'));
            let rowSpan = Math.ceil((post.querySelector('.content').getBoundingClientRect().height+rowGap)/(rowHeight+rowGap));
            post.style.gridRowEnd = "span "+rowSpan;
        })
}

const renderPost = () => {
    if (document.querySelector('post')){
        document.querySelector('post').remove();
    }
    let posts = document.querySelector('.posts');
    if (getData != null) {
        getData.forEach((item) => {
            let post = document.createElement('div');
                post.className = "post";
                post.dataset.id = item.id;
                post.innerHTML = `
                    <div class="content">
                        <img src="${item.image}" alt="">
                        <div class="title">
                            <h5>${item.title}</h5>
                        </div>
                        <div class="desc">
                            <p class="body1">
                                ${item.text}
                            </p>
                            <span class="body2 likes">
                                Likes: ${item.likes}   
                            </span>
                            <span class="body2 likes">
                             | 
                            </span>
                            <span class="body2">
                                Comments: ${item.commentsCounter  }
                            </span>
                            <p class="caption date">
                                ${item.date}
                            </p>
                        </div>
                        <div class="post-navigation">
                            <button class="author-button">${item.author}</button>
                            <button class="comment-button">Comm</button>
                            <button class="like-button">Like</button>
                        </div>
                    </div>
                 `
                post.querySelector('.like-button').addEventListener('click', addLike(post));
                post.querySelector('img').addEventListener('click', openImage(post) );

                post.querySelector('.comment-button').addEventListener('click', addComment(post));
                post.querySelector('.comment-button').addEventListener('click', renderComment(post));
                
                posts.appendChild(post);
        }) 
    }
    sizePost();
}

window.addEventListener("resize", sizePost);


export default renderPost;