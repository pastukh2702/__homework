
let open = false;

let popup = document.querySelector('.popup_image');
let img = popup.querySelector('img');
let popupBg = document.querySelector('.popup_bg');

const openImage = (post) => () => {
    if ( open == false) {
        popup.style = 'display: flex;';
        img.src = post.querySelector('img').src;
        popupBg.style = 'display: flex;';
        open = true;
        document.body.style = "overflow: hidden;"
        popup.addEventListener('click', () => {
            popup.style = 'display: none;';
            popupBg.style = 'display: none;'
            document.body.style = "overflow: auto;"
            open = false;
        })
        popupBg.addEventListener('click', () => {
            popup.style = 'display: none;';
            popupBg.style = 'display: none;'
            document.body.style = "overflow: auto;"
            open = false;
        })
    }
}

export default openImage;