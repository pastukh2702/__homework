import * as helpers from '../helpers/import';

let data = helpers.Data;
let getData = data.getData();

const addLike = (post) => (e) => {
    getData.forEach((el) => {
        if (el.id == post.dataset.id){
            let likes = post.querySelector('.likes');
                el.likes += 1;
                likes.innerHTML = `Likes: ${el.likes}`;
            data.setData(getData);
        }
    })
}

export default addLike;