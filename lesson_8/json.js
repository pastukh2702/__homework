
/*
  Задание:
  Написать скрипт который:
    1. Собирает данные с формы (3 разных полей), конвертирует их в json и выводит в консоль.
  ->  2. Сделать отдельный инпут который выполняет JSON.parse(); на ту строку что вы туда ввели и выводит результат в консоль.

  Array.from(HTMLNodeColection); -> Arary

  <form>
    <input name="name" />
    <input name="age"/>
    <input name="password"/>
    <button></button>
  </form>

  <input />

  -> '{"name" : !"23123", "age": 15, "password": "*****" }'


*/
let name = document.getElementById('name');
let age = document.getElementById('age');
let password = document.getElementById('password');
let dataObject;

let jsonInput = document.getElementById('jsoninput');

let buttonToJSON = document.getElementById('json');
    buttonToJSON.addEventListener('click', (e) => {
      e.preventDefault();
        dataObject = {
          name: name.value,
          age: age.value,
          password: password.value
        };
        dataObject = JSON.stringify(dataObject);
        jsonInput.value = dataObject;
        console.log(dataObject);  
});

let buttonToObject = document.getElementById('object');
    buttonToObject.addEventListener('click', (e) => {
      e.preventDefault();
        dataObject = JSON.parse(jsonInput.value);
        console.log(dataObject);
});








