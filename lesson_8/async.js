/*

  Задание:

    Написать при помощи async-await скрипт, который:

    Получает список компаний:  http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
    Перебирает, выводит табличку:

    # | Company  | Balance | Показать дату регистрации | Показать адресс |
    1.| CompName | 2000$   | button                    | button
    2.| CompName | 2000$   | 20/10/2019                | button
    3.| CompName | 2000$   | button                    | button
    4.| CompName | 2000$   | button                    | button

    Данные о дате регистрации и адресс скрывать при выводе и показывать при клике.

*/
const showAddress = (company, address) => () => {
  if(address.querySelector('div')){
    address.querySelector('div').remove();
  }else{
    let div = document.createElement('div');
    div.innerHTML = `
        Country: ${company.address.country}<br>
        City: ${company.address.city}<br>
        Street: ${company.address.street}<br>
        House: ${company.address.house}<br>
    `;
    address.appendChild(div);
  }
}

const showDate = (company, date) => () => {
  if(date.querySelector('div')){
    date.querySelector('div').remove();
  }else{
    let div = document.createElement('div');
    div.innerHTML = `
        Registration: ${company.registered}
    `;
    date.appendChild(div);
  }
}

const render = ( companys ) => {
  let table = document.querySelector('table');
  companys.forEach((company, index)=>{
    let tr = document.createElement('tr');

    let number = document.createElement('th');
        number.innerHTML = index+1;
    let companyName = document.createElement('th');
        companyName.innerHTML = company.company;
    let balance = document.createElement('th');
        balance.innerHTML = company.balance;
        
    let date = document.createElement('th');
        date.innerHTML = company.registered;
        date.innerHTML = `<button>show date</button>`;
        date.querySelectorAll('button').forEach((button) => button.addEventListener('click', showDate ( company, date )));

    let address = document.createElement('th');
        address.classList.add('address');
        address.innerHTML = `<button>show address</button>`;
        address.querySelectorAll('button').forEach((button) => button.addEventListener('click', showAddress ( company, address )));

    tr.appendChild(number);
    tr.appendChild(companyName);
    tr.appendChild(balance);
    tr.appendChild(date);
    tr.appendChild(address);

    table.appendChild(tr);
  })
}

async function Company() {
  const getCompanyResponse = await fetch('http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2');
  const companys = await getCompanyResponse.json();
  render(companys);
}


Company();
